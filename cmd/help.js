const explain = require("../explain");

module.exports = function (bot, msg) {
    args = msg.content.split(' ');
    if (args.length == 2 && args[0] == "!help") {
        cmd = args[1];
        if (Object.keys(explain).includes(cmd))
            explain[cmd](bot, msg.channel.id);
        else bot.createMessage(msg.channel.id, {
            embed: {
                color: 0xFF0000,
                title: "By golly!",
                description: "The `" + cmd + "` command doesn't have a detailed explanation!",
                footer: {
                    text: "Please speak with an admin if you require further assistance."
                }
            }
        });

        return;
    }

    bot.createMessage(msg.channel.id, {
        embed: {
            color: 0xB3924A,
            title: "**Clockwork Cowboy Commands**",
            fields: [
                {
                    name: "!help <command>",
                    value: "Get a more in-depth explanation of a command",
                    inline: false
                },
                {
                    name: "!play <character>",
                    value: "Begin playing as one of your characters",
                    inline: false
                },
                {
                    name: "!new",
                    value: "Create a new character. The bot will dm you.",
                    inline: false
                },
                {
                    name: "!tutorial",
                    value: "Start a speedrun tutorial of how to rp here, and use the bot.",
                    inline: false
                },
                {
                    name: "!list",
                    value: "List all your characters",
                    inline: false
                },
                {
                    name: "!about <character/user>",
                    value: "get info on a character, or a fellow player",
                    inline: false
                },
                {
                    name: "!stats",
                    value: "See your stats such as balance, and health",
                    inline: false
                },
                {
                    name: "!time",
                    value: "see the current world time",
                    inline: false
                },
                {
                    name: "!bag",
                    value: "see your inventory. use `!bag help` for more",
                    inline: false
                },
                {
                    name: "!trade <character>",
                    value: "trade with another character",
                    inline: false
                },
                {
                    name: "!roll <number of die> x <dice face count>",
                    value: "[Alias `!r`] Roll a dice (or more)",
                    inline: false
                }
            ],
            footer: {
                text: "Ask an admin for further assistance."
            }
        }
    });
};
