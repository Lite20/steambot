module.exports = function (bot, msg) {
    let id = msg.author.id;
    if (msg.mentions.length > 0) id = msg.mentions[0].id;
    bot.createMessage(msg.channel.id, {
        embed: {
            color: 0xee6570,
            fields: [
                {
                    name: "Guild ID",
                    value: msg.channel.guild.id,
                    inline: false
                },
                {
                    name: "Channel ID",
                    value: msg.channel.id,
                    inline: false
                },
                {
                    name: "User ID",
                    value: id,
                    inline: false
                }
            ]
        }
    });
};
