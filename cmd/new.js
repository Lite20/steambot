const explain = require("../explain");

function question(bot, channel_id, category, question_number, description) {
    bot.createMessage(channel_id, {
        embed: {
            color: 0xB3924A,
            title: "**" + category + "**",
            description: description,
            footer: {
                text: "Question " + question_number + "/10"
            }
        }
    });
}

function serialize(bot, channel_id, char, opts) {
    bot.createMessage(channel_id, {
        embed: {
            color: 0xB3924A,
            title: opts.title,
            description: opts.description,
            fields: [
                { name: "Name", value: char.name, inline: true },
                { name: "Age", value: char.age, inline: true },
                { name: "Prefix", value: char.prefix, inline: true },
                { name: "Augmentations", value: char.augmentations, inline: false },
                { name: "Weapons", value: char.weapons, inline: false },
                { name: "Personality", value: char.personality, inline: false },
                { name: "Race", value: char.race, inline: false },
                { name: "Appearance", value: char.appearance, inline: false },
                { name: "Background", value: char.background, inline: false },
                { name: "Strengths", value: char.strengths, inline: false },
                { name: "Notes", value: char.notes, inline: false }
            ],
            footer: {
                text: opts.footer
            }
        }
    });
}

let creator = {
    Creations: {},
    Process: (bot, msg) => {
        if (msg.content.toLowerCase().startsWith("!skip"))
            creator.Creations[msg.channel.id].resolve("<skipped>");
        else if (msg.content.toLowerCase().startsWith("!quit")) {
            creator.Creations[msg.channel.id].reject();
            delete creator.Creations[msg.channel.id];
        } else {
            creator.Creations[msg.channel.id].resolve(msg.content);
        }
    },
    Edit: (bot, msg) => {
        if (!creator.Creations.hasOwnProperty(msg.channel.id)) return;

        let char = creator.Creations[msg.channel.id];
        let prop = msg.content.split(" ")[1];
        let value = msg.content.substring(7 + prop.length);
        
        if (!char.hasOwnProperty(prop)) return bot.createMessage(msg.channel.id, {
            embed: {
                title: "**Huh? Something's not right there...**",
                description: "The syntax for editing a property is `!edit <property> <value>`. The properties available for editing are, `name, age, prefix, augmentations, weapons, personality, race, appearance, background, notes`. "
            }   
        });

        bot.createMessage(msg.channel.id, {
            embed: {
                title: "**Edit \"" + prop + "\"**",
                description: prop + " has now been changed.",
                fields: [
                    { name: "Previous:", value: char[prop], inline: true },
                    { name: "Now:", value: value, inline: true }
                ]
            }
        });

        char[prop] = value;
    },
    Done: (bot, msg, config) => {
        if (!creator.Creations.hasOwnProperty(msg.channel.id)) return;

        let char = creator.Creations[msg.channel.id];

        let unset = [];
        Reflect.ownKeys(char).forEach(key => {
            if (char[key] == "<skipped>") unset.push(key);
        });

        if (unset.length > 0) {
            bot.createMessage(msg.channel.id, {
                embed: {
                    color: 0xFF0000,
                    title: "**Hold on there, Partner!**",
                    description: "The following properties were skipped and have not yet been set! Please set them with `!edit <property> <what to make it>`: " + unset.join(", "),
                    footer: {
                        text: "Please message an moderator if you need help."
                    }
                }
            });
        } else {
            bot.createMessage(msg.channel.id, {
                embed: {
                    color: 0xB3924A,
                    title: "**Congratulations!**",
                    description: "Your character has been submitted! You should hear from a moderator or two soon. Should you feel you need to adjust something now, simply tell a mod what you'd like adjusted!",
                    footer: {
                        text: "Please message an moderator if you need help."
                    }
                }
            });
            
            serialize(bot, config.CHANNEL.SUBMISSION, char, {
                title: "**Submission [" + msg.channel.id + "] Inbound!**",
                description: msg.author.username + " has submitted a character. Please review their submission!",
                footer: "Please message mitsu#0001 if you need help."
            });

            bot.createMessage(
                config.CHANNEL.SUBMISSION,
                "<@&578381390261780480> Use `!approve " + msg.channel.id + "` to approve this submission."
            );
        }
    },
    Handle: (bot, msg) => {
        let dm;
        msg.author.getDMChannel().then((dm_channel) => {
            dm = dm_channel;
            creator.Creations[dm.id] = {};
            return bot.createMessage(
                msg.channel.id,
                "*I'll meet you in your direct messages <@" + msg.author.id + ">!*"
            );
        }).then(() => {
            return bot.createMessage(dm.id, {
                embed: {
                    color: 0xB3924A,
                    title: "**Welcome to the character creator!**",
                    description: "I will guide you through the making of your character. Type `!quit` at any point to abort. If you make a mistake, you can change things at the end. If you haven't thought of the answer to a question yet, just type `!skip`.",
                    footer: {
                        text: "Please message an moderator if you need help."
                    }
                }
            });
        }).then(() => {
            question(
                bot, dm.id, "Character Name", "1", "Please write out the full name of your character."
            );

            return awaitResponse(dm.id);
        }).then((name) => {
            creator.Creations[dm.id].name = name;

            question(
                bot, dm.id, "Character Age", "2",
                "Next, please write your character's age. You can write words here if there's something to note about their age."
            );

            return awaitResponse(dm.id);
        }).then((age) => {
            creator.Creations[dm.id].age = age;

            question(
                bot, dm.id, "Gender Prefix", "3",
                "Next, please specify your characters gender prefix. Do they prefer to be called \"he\", \"she\", \"they\", or something else? You may specify multiple if need be."
            );

            return awaitResponse(dm.id);
        }).then((prefix) => {
            creator.Creations[dm.id].prefix = prefix;

            question(
                bot, dm.id, "Augmentations", "4",
                "Alrighty. Does your character have any augmentations? A bionic leg? A propellor hat? Do they maybe have a cyborg dog?"
            );

            return awaitResponse(dm.id);
        }).then((augmentations) => {
            creator.Creations[dm.id].augmentations = augmentations;

            question(
                bot, dm.id, "Weapons", "5",
                "Oh, I see! How about weapons? What weapons you got?"
            );

            return awaitResponse(dm.id);
        }).then((weapons) => {
            creator.Creations[dm.id].weapons = weapons;

            question(
                bot, dm.id, "Personality", "6",
                "Ah, that's perfect! What traits would you say describe your character?"
            );

            return awaitResponse(dm.id);
        }).then((personality) => {
            creator.Creations[dm.id].personality = personality;

            question(
                bot, dm.id, "Personality", "6.3",
                "Hmm, okay! What are their likes and dislikes?"
            );

            return awaitResponse(dm.id);
        }).then((personality) => {
            if (personality != "<skipped>") creator.Creations[dm.id].personality += personality;

            question(
                bot, dm.id, "Personality", "6.6",
                "Sounds good! What will other characters likely think first when they encounter your character?"
            );

            return awaitResponse(dm.id);
        }).then((personality) => {
            if (personality != "<skipped>") creator.Creations[dm.id].personality += personality;

            question(
                bot, dm.id, "Race", "7",
                "Alright! I wonder if it will play out that way. Easy question: what race is your character?"
            );

            return awaitResponse(dm.id);
        }).then((race) => {
            creator.Creations[dm.id].race = race;

            question(
                bot, dm.id, "Appearance.", "8",
                "Yeah, that was too easy. Okay, you get to be creative here. Describe their appearance for me. Hair style/color, height, markings, accent, build? Things like that."
            );

            return awaitResponse(dm.id);
        }).then((appearance) => {
            creator.Creations[dm.id].appearance = appearance;

            question(
                bot, dm.id, "Background", "9",
                "Got it! Alright, now write me something *long* here. Tell me about your characters background- their past. What have they been through? Where are they now? Any life goals?"
            );

            return awaitResponse(dm.id);
        }).then((background) => {
            creator.Creations[dm.id].background = background;

            question(
                bot, dm.id, "Strengths/Weaknesses", "10",
                "Hmm, okay I see; thanks! We're almost done. Tell me their strengths and weaknesses. Remember to keep things balanced!"
            );

            return awaitResponse(dm.id);
        }).then((strengths) => {
            creator.Creations[dm.id].strengths = strengths;

            question(
                bot, dm.id, "Foot Notes", "-",
                "Sweet work! Alright, this last area is entirely for you. Do you have any notes to make to other players, or to the moderators?"
            );

            return awaitResponse(dm.id);
        }).then((notes) => {
            creator.Creations[dm.id].notes = notes;
            let char = creator.Creations[dm.id];
            serialize(bot, dm.id, char, {
                title: "**Woohoo! All done!**",
                description: "So this is everything you've told me thus far. To make a change use the command `!edit <property> <what to change it to>`. When you're done, type `!done`!",
                footer: "Please message an moderator if you need help."
            });

        }).catch((err) => {
            console.log(err);
        });
    }
};

function awaitResponse(channel_id) {
    return new Promise(function (resolve, reject) {
        creator.Creations[channel_id].resolve = resolve;
        creator.Creations[channel_id].reject = reject;
    });
}

module.exports = creator;