const explain = require("../explain");

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
}

function fail(bot, channel_id, error) {
    bot.createMessage(channel_id, {
        embed: {
            color: 0xFF0000,
            title: error,
            description: "!roll `<number of die>` d `<number of faces>` x `[number of times]` `[add/sub/avg]` `[adv/dis]`",
            footer: {
                text: "Use \"!help roll\" for more help with the command."
            }
        }
    });
}

const arrSum = arr => arr.reduce((a, b) => a + b, 0);
const arrDiff = arr => arr.reduce((a, b) => a - b, 0);
const arrAvg = arr => arr.reduce((a, b) => a + b, 0) / arr.length;

module.exports = function (bot, msg, aliased) {
    let msg_no_cmd = msg.content.substring((aliased ? 3 : 6));
    let msg_no_flags = msg_no_cmd.replace("adv", '')
        .replace("dis", '')
        .replace("add", '')
        .replace("sub", '')
        .replace("avg", '')
        .replace(' ', '');

    let avg = false;
    let combo_state = 0;

    // parse command
    if (msg_no_cmd.includes("avg")) avg = true;

    if (msg_no_cmd.includes("adv")) combo_state = 1;
    else if (msg_no_cmd.includes("dis")) combo_state = 2;

    let d_parts = msg_no_flags.split('d');
    if (d_parts.length != 2) return fail(bot, msg.channel.id, "Double check your syntax!");
    let d_string = (d_parts[0] || "1");
    let x_parts = d_parts[1].split('x');
    let f_string = x_parts[0];
    let x_string = (x_parts[1] || "1");
    if (isNaN(d_string) || isNaN(f_string) || isNaN(x_string))
        return fail(bot, msg.channel.id, "Double check your syntax!");

    let dice_count, face_count, roll_times;
    dice_count = parseInt(d_string);
    face_count = parseInt(f_string);
    roll_times = parseInt(x_string);

    if (combo_state != 0 && roll_times < 2) roll_times = 2;
    
    // perform checks
    if (roll_times > 20) {
        bot.createMessage(msg.channel.id, {
            embed: {
                color: 0xFF0000,
                title: "Hold on, partner!",
                description: "You can only roll 20 times in one go.",
                footer: {
                    text: "Use \"!help roll\" for more help with the command."
                }
            }
        });

        return;
    }

    if (face_count <= 0) {
        fail(bot, msg.channel.id, "Huh? A " + face_count + " faced dice?");
        return;
    }

    if (dice_count <=0) {
        fail(bot, msg.channel.id, dice_count + " dice? Did I miss something?");
        return;
    }

    if (roll_times <= 0) {
        fail(bot, msg.channel.id, roll_times + " rolls? Uh, okay.");
        return;
    }

    // perform rolls
    let results = [];
    for (let i = 0; i < roll_times; i++) {
        let set = [];
        for (let x = 0; x < dice_count; x++)
            set.push(getRandomIntInclusive(1, face_count));
        
        let final;
        if (avg) final = arrAvg(set);
        else final = arrSum(set);

        results.push(final);
    }

    // generate output based on whether maximizing, minimizing, or listing
    let output = results.join(", ");
    if (combo_state == 1) {
        let max = Math.max(...results);
        output = output.replace(max, "**" + max + "**");
    } else if(combo_state == 2) {
        let min = Math.min(...results);
        output = output.replace(min, "**" + min + "**");
    }
    
    bot.deleteMessage(msg.channel.id, msg.id, "house cleaning");
    bot.createMessage(msg.channel.id, {
        embed: {
            color: 0xFFD14E,
            title: (msg.member.nick || msg.author.username) + " rolls:",
            description: output,
            footer: {
                text: msg.content
            }
        }
    });
};
