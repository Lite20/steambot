function present(bot, channel_id, args) {
    // construct field data
    field_data = [{
        name: "About", value: args.about, inline: false
    }, {
        name: "Syntax", value: args.syntax, inline: false
    }];

    // add examples
    args.examples.forEach(element => {
        field_data.push({
            name: "Example", value: element, inline: false
        });
    });

    // append notes
    field_data.push({
        name: "Notes", value: args.notes, inline: false
    });

    bot.createMessage(channel_id, {
        embed: {
            color: 0xB3924A,
            title: "Help for \"!" + args.cmd + "\"",
            fields: field_data,
            footer: {
                text: "Ask an admin for further assistance."
            }
        }
    });
}

module.exports = {
    roll: function(bot, channel_id) {
        present(bot, channel_id, {
            cmd: "roll",
            syntax: "!roll `<number of die>` d `<number of faces>` x `[number of times]` *`[avg]` `[adv/dis]`*",
            about: "The dice command lets you roll a number of virtual die, several times.",
            examples: [
                "Roll one 6-faced die: `!roll 1d6`",
                "Roll one 20-faced die. Repeated twice: `!roll 1d20x2`",
                "Roll with advantage, using two 8-faced die: `!roll 2d8 adv`",
                "Roll ten 100-faced die and average their results: `!roll 10d100 avg`"
            ],
            notes: "Numbers are added together, unless specified otherwise (with `avg`). You can roll a maximum of 20 times at once."
        });
    }
}