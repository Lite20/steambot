const fs = require('fs');
const Eris = require("eris");
const nano = require('nano')('http://localhost:5984');

const Config = require("./config.json");

const Help = require("./cmd/help");
const Roll = require("./cmd/roll");
const New = require("./cmd/new");
const ID = require("./cmd/id");

var bot = new Eris(Config.TOKEN, {
    largeThreshold: 1 // how many offline users to grab
});

var user_db, char_db;
nano.db.create('steam_user', () => {
    user_db = nano.use('steam_user');
});

nano.db.create('steam_char', () => {
    char_db = nano.use('steam_char');
});

bot.on("ready", () => {
    console.log("Connected to Discord");
    bot.editStatus("online", { name: "ridding snake from boot", type: 0 });

    setInterval(() => {
        bot.editStatus("online", { name: "ridding snek from boot", type: 0 });
    }, 120000)
});

bot.on("messageCreate", (msg) => {
    if (msg.author.bot) return;
    if (msg.channel.id == "580794637887864858") return;

    msg.content_lower = msg.content.toLowerCase();

    // dev commands
    if (msg.content.startsWith("!id")) ID(bot, msg);
    // public commands
    else if (msg.content_lower.startsWith("!help"))  Help(bot, msg);
    else if (msg.content_lower.startsWith("!new"))   New.Handle(bot, msg);
    else if (msg.content_lower.startsWith("!edit"))  New.Edit(bot, msg);
    else if (msg.content_lower.startsWith("!done"))  New.Done(bot, msg, Config);
    else if (msg.content_lower.startsWith("!list"))  Help(bot, msg);
    else if (msg.content_lower.startsWith("!about")) Help(bot, msg);
    else if (msg.content_lower.startsWith("!stats")) Help(bot, msg);
    else if (msg.content_lower.startsWith("!time"))  Help(bot, msg);
    else if (msg.content_lower.startsWith("!bag"))   Help(bot, msg);
    else if (msg.content_lower.startsWith("!trade")) Help(bot, msg);
    else if (msg.content_lower.startsWith("!roll"))  Roll(bot, msg, false);
    else if (msg.content_lower.startsWith("!r"))     Roll(bot, msg, true);
    // mod commands
    else if (msg.content_lower.startsWith("!ban"))    Help(bot, msg);
    else if (msg.content_lower.startsWith("!warn"))   Help(bot, msg);
    else if (msg.content_lower.startsWith("!freeze")) Help(bot, msg);
    else if (msg.content_lower.startsWith("!mask"))   Help(bot, msg);
    // check if is response to a character creation sequence
    if (New.Creations.hasOwnProperty(msg.channel.id)) return New.Process(bot, msg);
});

bot.on("guildMemberAdd", (guild, member) => {
    if (member.bot) return;
    bot.createMessage(Config.WELCOME_CHANNEL, Config.WELCOME_MESSAGE.replace('USR_ID', member.id));
});

bot.connect();
